package me.x3n.UIRecognition;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class RenderUtils {
    public static BufferedImage renderWindow(RootPaneContainer window) {
        JRootPane rootPane = window.getRootPane();
        BufferedImage image = new BufferedImage(
                rootPane.getWidth(),
                rootPane.getHeight(),
                BufferedImage.TYPE_3BYTE_BGR
        );
        rootPane.paint(image.getGraphics());
        return image;
    }

    public static BufferedImage renderWindowScaled(RootPaneContainer window, double scaleFactor) {
        Container contentPane = window.getContentPane();

        int width = (int) (contentPane.getWidth() * scaleFactor);
        int height = (int) (contentPane.getHeight() * scaleFactor);

        BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR);
        Graphics2D g2d = img.createGraphics();

        g2d.setTransform(AffineTransform.getScaleInstance(scaleFactor, scaleFactor));
        contentPane.paint(g2d);
        g2d.dispose();

        return img;
    }

    public static void createWindowScreenshot(JWindow window, String filename) throws AWTException, IOException {
        Robot robot = new Robot();

        BufferedImage screenshotRaw = robot.createScreenCapture(window.getBounds());
        BufferedImage screenshot = new BufferedImage(
                screenshotRaw.getWidth(), screenshotRaw.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
        screenshot.getGraphics().drawImage(screenshotRaw, 0, 0, null);

        ImageIO.write(screenshot, "png", new File(filename));
    }
}
