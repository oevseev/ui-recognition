package me.x3n.UIRecognition.matchers.darcula;

import me.x3n.UIRecognition.ComponentMatcher;
import org.bytedeco.javacv.Java2DFrameUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import static org.bytedeco.javacpp.opencv_core.*;
import static org.bytedeco.javacpp.opencv_imgproc.*;

public class ReferenceMatcher<T extends JComponent> extends ComponentMatcher<T> {
    private static final double THRESHOLD = 0.999;
    private T referenceComponent;

    public ReferenceMatcher(T component, T referenceComponent) {
        super(component);
        this.referenceComponent = referenceComponent;
    }

    private BufferedImage renderReferenceComponent() {
        JFrame frame = new JFrame();
        frame.setContentPane(referenceComponent);
        frame.pack();
        BufferedImage img = new BufferedImage(
                referenceComponent.getWidth(),
                referenceComponent.getHeight(),
                BufferedImage.TYPE_3BYTE_BGR
        );
        frame.getRootPane().paint(img.getGraphics());
        return img;
    }

    @Override
    protected List<Rectangle> doGetPossibleBoundingBoxes(
            BufferedImage image, BufferedImage hiResReference, double scalingFactor) {
        Mat img = Java2DFrameUtils.toMat(image);
        BufferedImage patternImg = renderReferenceComponent();
        Mat pattern = Java2DFrameUtils.toMat(patternImg);
        Mat result = new Mat();

        matchTemplate(img, pattern, result, TM_CCORR_NORMED);

        ArrayList<Rectangle> candidates = new ArrayList<>();
        for (int i = 0; i < result.rows(); i++) {
            for (int j = 0; j < result.cols(); j++) {
                double score = result.ptr(i, j).getFloat();
                if (score > THRESHOLD) {
                    candidates.add(new Rectangle(j, i, pattern.cols(), pattern.rows()));
                }
            }
        }

        return candidates;
    }
}
