package me.x3n.UIRecognition.matchers.darcula;

import me.x3n.UIRecognition.ComponentMatcher;
import me.x3n.UIRecognition.ComponentMatcherFactory;
import me.x3n.UIRecognition.SwingComponentMatcherFactory;

import javax.swing.*;

import static org.bytedeco.javacpp.tesseract.*;

public class DarculaComponentMatcherFactory extends SwingComponentMatcherFactory {

    public DarculaComponentMatcherFactory() {

    }

    @Override
    protected ComponentMatcher constructButtonMatcher(JButton button) {
        return new ButtonMatcher(button);
    }

    @Override
    protected ComponentMatcher constructLabelMatcher(JLabel label) {
        return new LabelMatcher(label);
    }

    @Override
    protected ComponentMatcher constructCheckBoxMatcher(JCheckBox checkBox) {
        JCheckBox cbRef = new JCheckBox(checkBox.getText());
        cbRef.setFont(checkBox.getFont());
        return new ReferenceMatcher<>(checkBox, cbRef);
    }

    @Override
    protected ComponentMatcher constructRadioButtonMatcher(JRadioButton radioButton) {
        JRadioButton rbRef = new JRadioButton(radioButton.getText());
        rbRef.setFont(radioButton.getFont());
        return new ReferenceMatcher<>(radioButton, rbRef);
    }

    @Override
    public ComponentMatcherFactory clone() {
        return new DarculaComponentMatcherFactory();
    }


}
