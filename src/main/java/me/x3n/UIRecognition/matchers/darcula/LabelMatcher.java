package me.x3n.UIRecognition.matchers.darcula;

import me.x3n.UIRecognition.ComponentMatcher;
import me.x3n.UIRecognition.LocalTesseract;
import org.bytedeco.javacpp.IntPointer;
import org.bytedeco.javacv.Java2DFrameUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import static org.bytedeco.javacpp.lept.*;
import static org.bytedeco.javacpp.opencv_core.*;
import static org.bytedeco.javacpp.opencv_imgproc.*;
import static org.bytedeco.javacpp.tesseract.*;

public class LabelMatcher extends ComponentMatcher<JLabel> {
    public LabelMatcher(JLabel label) {
        super(label);
    }

    @Override
    protected List<Rectangle> doGetPossibleBoundingBoxes(BufferedImage image, BufferedImage hiResReference, double scaleFactor) {
        Mat img = Java2DFrameUtils.toMat(image);
        Mat reference = Java2DFrameUtils.toMat(hiResReference != null ? hiResReference : image);

        TessBaseAPI api = LocalTesseract.getLocalTessBaseAPI();

        byte[] data = LocalTesseract.bufferedImageToPngWithDPI(image, 300);
        PIX pix = pixReadMemPng(data, data.length);
        api.SetImage(pix);

        PIXA pixa = new PIXA();
        int[] blockIds = new int[image.getWidth() * image.getHeight()];
        BOXA boxa = api.GetComponentImages(RIL_WORD, true, pixa, new IntPointer());

        Mat mat = Mat.zeros(new Size(image.getWidth(), image.getHeight()), CV_8UC1).asMat();
        for (int i = 0; i < boxa.n(); i++) {
            BOX box = boxa.box(i);
            rectangle(mat, new Rect(box.x(), box.y(), box.w(), box.h()), new Scalar(255.0), CV_FILLED, LINE_8, 0);
        }

        pixDestroy(pix);
        pixaDestroy(pixa);
        boxaDestroy(boxa);

        dilate(mat, mat, getStructuringElement(CV_SHAPE_RECT, new Size(15, 1)));
        erode(mat, mat, getStructuringElement(CV_SHAPE_RECT, new Size(15, 1)));
        dilate(mat, mat, new Mat());

        MatVector contours = new MatVector();
        Mat hierarchy = new Mat();
        findContours(mat, contours, hierarchy, RETR_CCOMP, CHAIN_APPROX_SIMPLE);

        /*
        Mat out = Java2DFrameUtils.toMat(image);
        drawContours(out, contours, -1, new Scalar(255.0, 255.0, 255.0, 255.0));

        BufferedImage outImg = Java2DFrameUtils.toBufferedImage(out);
        return new ArrayList<>();
        */

        ArrayList<Rectangle> candidates = new ArrayList<>();

        for (int i = 0; i < contours.size(); i++) {
            Rect rect = boundingRect(contours.get(i));
            Rect scaledRect = new Rect(
                    (int) (rect.x() * scaleFactor),
                    (int) (rect.y() * scaleFactor),
                    (int) (rect.width() * scaleFactor),
                    (int) (rect.height() * scaleFactor)
            );
            Mat label = reference.apply(scaledRect).clone();
            bitwise_not(label, label);
            BufferedImage labelImg = Java2DFrameUtils.toBufferedImage(label);

            String text = LocalTesseract.detectText(labelImg);
            if (text.equals(component.getText())) {
                candidates.add(new Rectangle(rect.x(), rect.y(), rect.width(), rect.height()));
            }
        }

        return candidates;
    }
}
