package me.x3n.UIRecognition.matchers.darcula;

import me.x3n.UIRecognition.ComponentMatcher;
import me.x3n.UIRecognition.LocalTesseract;
import org.bytedeco.javacv.Java2DFrameUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import static org.bytedeco.javacpp.opencv_core.*;
import static org.bytedeco.javacpp.opencv_imgproc.*;
import static org.bytedeco.javacpp.tesseract.TessBaseAPI;

public class ButtonMatcher extends ComponentMatcher<JButton> {
    private static final double MIN_AREA = 200.0;

    public ButtonMatcher(JButton button) {
        super(button);
    }

    @Override
    protected List<Rectangle> doGetPossibleBoundingBoxes(
            BufferedImage image, BufferedImage hiResReference, double scaleFactor) {
        Mat img = Java2DFrameUtils.toMat(image);
        Mat reference = Java2DFrameUtils.toMat(hiResReference != null ? hiResReference : image);

        Mat edges = new Mat();
        cvtColor(img, edges, COLOR_BGR2GRAY);
        adaptiveThreshold(edges, edges, 255, ADAPTIVE_THRESH_GAUSSIAN_C, THRESH_BINARY, 3, -1);

        Mat horz_lines = new Mat(), vert_lines = new Mat();
        Mat horz_kernel = getStructuringElement(MORPH_RECT, new Size(15, 1));
        Mat vert_kernel = getStructuringElement(MORPH_RECT, new Size(1, 15));

        erode(edges, horz_lines, horz_kernel);
        dilate(horz_lines, horz_lines, horz_kernel);
        erode(edges, vert_lines, vert_kernel);
        dilate(vert_lines, vert_lines, vert_kernel);

        Mat lines = new Mat();
        bitwise_or(horz_lines, vert_lines, lines);
        dilate(lines, lines, new Mat());

        MatVector contours = new MatVector();
        Mat hierarchy = new Mat();
        findContours(lines, contours, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE);

        int numContours = (int) contours.size();
        int[] childrenCount = new int[numContours];
        boolean[] degenerateRect = new boolean[numContours];

        for (int i = 0; i < numContours; i++) {
            int parent = hierarchy.ptr(0, i).getInt(12);
            Rect rect = boundingRect(contours.get(i));
            if (rect.area() > MIN_AREA) {
                if (parent >= 0) {
                    childrenCount[parent]++;
                }
            }
            else {
                degenerateRect[i] = true;
            }
        }

        ArrayList<Rect> rects = new ArrayList<>();
        for (int i = 0; i < numContours; i++) {
            if (childrenCount[i] == 0 && !degenerateRect[i]) {
                rects.add(boundingRect(contours.get(i)));
            }
        }

        TessBaseAPI api = LocalTesseract.getLocalTessBaseAPI();
        ArrayList<Rectangle> candidates = new ArrayList<>();

        for (Rect rect : rects) {
            Rect scaledRect = new Rect(
                    (int) (rect.x() * scaleFactor),
                    (int) (rect.y() * scaleFactor),
                    (int) (rect.width() * scaleFactor),
                    (int) (rect.height() * scaleFactor)
            );
            Mat resizedButton = reference.apply(scaledRect).clone();
            bitwise_not(resizedButton, resizedButton);
            BufferedImage resizedButtonImg = Java2DFrameUtils.toBufferedImage(resizedButton);

            String text = LocalTesseract.detectText(resizedButtonImg);

            boolean match = false;
            if (text.contains("...")) {
                String[] parts = text.split("\\.\\.\\.", 2);
                if (component.getText().startsWith(parts[0]) && component.getText().endsWith(parts[1])) {
                    match = true;
                }
            }
            else if (text.equals(component.getText())) {
                match = true;
            }

            if (match) {
                candidates.add(new Rectangle(rect.x(), rect.y(), rect.width(), rect.height()));
            }
        }

        return candidates;
    }
}
