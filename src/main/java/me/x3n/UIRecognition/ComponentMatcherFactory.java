package me.x3n.UIRecognition;

import javax.swing.*;

public class ComponentMatcherFactory {
    public final ComponentMatcher constructMatcher(JComponent component) {
        if (component instanceof ValidatableComponent) {
            return ((ValidatableComponent) component).constructComponentFinder();
        }
        ComponentMatcher f;
        if ((f = doConstructMatcher(component)) != null) {
            return f;
        }
        throw new IllegalArgumentException(
                String.format("Don't know how to handle %s", component.getClass().getName()));
    }

    protected ComponentMatcher doConstructMatcher(JComponent component) {
        return null;
    }

    public ComponentMatcherFactory clone() {
        return new ComponentMatcherFactory();
    }
}
