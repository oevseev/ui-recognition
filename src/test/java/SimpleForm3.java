import javax.swing.*;
import java.util.Arrays;
import java.util.List;

public class SimpleForm3 implements ValidatableForm {
    private JPanel panel;
    private JTextField hostField;
    private JTextField portField;
    private JTextField fileField;
    private JTextField usernameField;
    private JButton browseButton;
    private JButton sendButton;
    private JPasswordField passwordField;
    private JLabel hostLabel;
    private JLabel usernameLabel;
    private JLabel portLabel;
    private JLabel passwordLabel;
    private JLabel fileLabel;

    public JPanel getPanel() {
        return panel;
    }
    @Override
    public List<JComponent> getComponentsToValidate() {
        return Arrays.asList(browseButton, sendButton, hostLabel, usernameLabel, passwordLabel, fileLabel, portLabel);
    }
}
