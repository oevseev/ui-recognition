import com.bulenkov.darcula.DarculaLaf;
import me.x3n.UIRecognition.LocalTesseract;
import me.x3n.UIRecognition.RenderUtils;
import me.x3n.UIRecognition.WindowValidator;
import me.x3n.UIRecognition.matchers.darcula.DarculaComponentMatcherFactory;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DemoTests {
    @BeforeAll
    static void initLaf() throws UnsupportedLookAndFeelException {
        UIManager.setLookAndFeel(new DarculaLaf());
        WindowValidator.setFactoryPrototype(new DarculaComponentMatcherFactory());
    }

    @Test
    void goodForm() {
        DemoForm demoForm = new DemoForm();
        validateForm(demoForm);
    }

    @Test
    void simpleForm1() {
        SimpleForm1 simpleForm1 = new SimpleForm1();
        validateForm(simpleForm1);
    }

    @Test
    void simpleForm2() {
        SimpleForm2 simpleForm2 = new SimpleForm2();
        validateForm(simpleForm2);
    }

    @Test
    void simpleForm3() {
        SimpleForm3 simpleForm3 = new SimpleForm3();
        validateForm(simpleForm3);
    }

    @Test
    void simpleForm4() {
        SimpleForm4 simpleForm4 = new SimpleForm4();
        validateForm(simpleForm4, 640, 480);
    }

    @Test
    void simpleForm5() {
        SimpleForm5 simpleForm5 = new SimpleForm5();
        validateForm(simpleForm5);
    }

    @Test
    void simpleForm6() {
        SimpleForm6 simpleForm6 = new SimpleForm6();
        validateForm(simpleForm6);
    }

    // @Test
    // void TrickyForm1() {
    //     TrickyForm1 trickyForm1 = new TrickyForm1();
    //     validateForm(trickyForm1);
    // }

    @Test
    void formOverflow() {
        DemoForm demoForm = new DemoForm();

        JFrame frame = new JFrame();
        frame.setContentPane(demoForm.getPanel());
        frame.pack();

        // FlowLayout will wrap the components to the next line so they will be drawn outside form bounds
        frame.setSize(frame.getWidth() - 50, frame.getHeight());
        // Unless the frame is explicitly made visible, size may not be immediately updated
        frame.setVisible(true);

        BufferedImage image = RenderUtils.renderWindow(frame);

        WindowValidator windowValidator = new WindowValidator(frame);
        for (JComponent component : demoForm.getComponentsToValidate()) {
            windowValidator.addComponentToValidate(component);
        }
        assertFalse(windowValidator.validateFull(image));

        frame.dispose();
    }

    @AfterAll
    static void cleanup() {
        LocalTesseract.endLocalTessBaseAPI();
    }

    private void validateForm(ValidatableForm form) {
        validateForm(form, null);
    }

    private void validateForm(ValidatableForm form, int width, int height) {
        validateForm(form, new Dimension(width, height));
    }

    private void validateForm(ValidatableForm form, Dimension dimension) {
        JFrame frame = new JFrame();
        frame.setContentPane(form.getPanel());
        if (dimension != null) {
            frame.setSize(dimension);
            frame.setVisible(true);
        }
        else {
            frame.pack();
        }

        BufferedImage image = RenderUtils.renderWindow(frame);
        WindowValidator windowValidator = new WindowValidator(frame);
        for (JComponent component : form.getComponentsToValidate()) {
            windowValidator.addComponentToValidate(component);
        }
        boolean result = windowValidator.validateFull(image);

        String randomName = "/Users/Eulyrus/" + UUID.randomUUID().toString() + ".png";
        try {
            ImageIO.write(windowValidator.visualizeBestConfiguration(), "png", new File(randomName));
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        assertTrue(result);

        frame.dispose();
    }
}
