import javax.swing.*;
import java.util.Arrays;
import java.util.List;

public class SimpleForm6 implements ValidatableForm {
    public JPanel getPanel() {
        return panel;
    }
    public List<JComponent> getComponentsToValidate() {
        return Arrays.asList(a2Button, a3Button, a5Button, a6Button, a1Button, a4Button, a7Button, a8Button, a9Button, a0Button);
    }

    private JButton a2Button;
    private JButton a3Button;
    private JButton a5Button;
    private JButton a6Button;
    private JButton a1Button;
    private JButton a4Button;
    private JButton a7Button;
    private JButton a8Button;
    private JButton a9Button;
    private JButton a0Button;
    private JPanel panel;
    private JTextArea a0TextArea;
}
