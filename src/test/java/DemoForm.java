import javax.swing.*;
import java.util.Arrays;
import java.util.List;

public class DemoForm implements ValidatableForm {
    public JPanel getPanel() {
        return panel;
    }

    public List<JComponent> getComponentsToValidate() {
        return Arrays.asList(testButton, anotherButton, label);
    }

    private JPanel panel;
    private JButton testButton;
    private JButton anotherButton;
    private JLabel label;
}
