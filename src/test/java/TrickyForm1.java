import javax.swing.*;
import java.util.Arrays;
import java.util.List;

public class TrickyForm1 implements ValidatableForm {
    public JPanel getPanel() {
        return panel;
    }
    public List<JComponent> getComponentsToValidate() {
        return Arrays.asList(a1Button1, a1Button, a2Button1, a2Button, a3Button1, a3Button, a4Button1, a4Button, a5Button1, a5Button, a6Button1, a6Button, a7Button1, a7Button, a8Button1, a8Button, a9Button1, a9Button, a10Button1, a10Button, a11Button1, a11Button, a12Button1, a12Button, a13Button1, a13Button, a14Button1, a14Button, a15Button1, a15Button, puzzletagLabel);
    }

    private JPanel panel;
    private JButton a1Button1;
    private JButton a1Button;
    private JButton a2Button1;
    private JButton a2Button;
    private JButton a3Button1;
    private JButton a3Button;
    private JButton a4Button1;
    private JButton a4Button;
    private JButton a5Button1;
    private JButton a5Button;
    private JButton a6Button1;
    private JButton a6Button;
    private JButton a7Button1;
    private JButton a7Button;
    private JButton a8Button1;
    private JButton a8Button;
    private JButton a9Button1;
    private JButton a9Button;
    private JButton a10Button1;
    private JButton a10Button;
    private JButton a11Button1;
    private JButton a11Button;
    private JButton a12Button1;
    private JButton a12Button;
    private JButton a13Button1;
    private JButton a13Button;
    private JButton a14Button1;
    private JButton a14Button;
    private JButton a15Button1;
    private JButton a15Button;
    private JLabel puzzletagLabel;
}
